module.exports = app => {
    const artist = app.controllers.artist
    app.route('/api/artist/:id')
        .get(artist.getArtistDetails);
    
    app.route('/api/artist/:id/musics')
        .get(artist.getMusicsByArtistId);
}