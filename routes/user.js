module.exports = app => {
    const users = app.controllers.user
    //const permission = require('../middlewares/permission')

    app.route('/api/user/:id/artists')
    //CASO NECESSÁRIO AUTENTICAÇÃO PARA A API PODEMOS COLOCAR UM MIDDLEWARE PARA CHECAR A PERMISSÁO, CONFORME EXEMPLO ABAIXO
        .get(users.findArtistsByUserId);

}