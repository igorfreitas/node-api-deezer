const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const load = require('express-load');
const expressValidator = require('express-validator');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());

load('repositories').then('controllers').then('routes').into(app);

module.exports = app;
