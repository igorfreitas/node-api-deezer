const axios =  require('axios')
module.exports = app => {
    const ArtistRepository = {
        getArtistDetails: (artistId) => {
            return new Promise(function(resolve, reject){
                axios.get(`${app.repositories.constantes.BASE_URL}/artist/${artistId}`).then(
                    res => resolve(res.data),
                    err => reject(err)
                )}
            )
        },
        getMusicsByArtistId: (artistId) => {
            return new Promise(function(resolve, reject){
                axios.get(`${app.repositories.constantes.BASE_URL}/artist/${artistId}/radio`).then(
                    res => resolve(res.data),
                    err => reject(err)
                )}
            )
        }
    }
    return ArtistRepository;
}