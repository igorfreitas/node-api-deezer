const axios =  require('axios')
module.exports = app => {

    const UserRepository = {
        findArtistsByUserId: (userId) => {
            return new Promise(function(resolve, reject){
                axios.get(`${app.repositories.constantes.BASE_URL}/user/${userId}/artists`).then(
                    res => resolve(res.data),
                    err => reject(err)
                )}
            )
        }
    }

    return UserRepository;
}