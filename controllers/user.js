module.exports = app => {
    const userRepository = app.repositories.userRepository;
    const UserController = {
        findArtistsByUserId: (req,res) => {
            userRepository.findArtistsByUserId(req.params.id).then(
                artistsData => {
                    if(artistsData.data){
                        res.status(200).json({
                            success: true,
                            artists: artistsData.data
                        })
                    }else{
                        res.json({
                            success: false,
                            error: artistsData.error
                        })
                    }
                    
                },
                err => {
                    res.status(403).json({
                        success: false,
                        error: `Erro na busca dos artistas: ${err}`
                    })
                }
            )
        }
    }
    return UserController;
}