module.exports = app => {
    const artistRepository = app.repositories.artistRepository;
    const ArtistController = {
        getArtistDetails: (req,res) => {
            artistRepository.getArtistDetails(req.params.id).then(
                artistData => {
                    if(artistData.id){
                        res.status(200).json({
                            success: true,
                            artistDetails: artistData
                        })
                    }else{
                        res.json({
                            success: false,
                            error: artistData.error
                        })
                    }
                    
                },
                err => {
                    res.status(403).json({
                        success: false,
                        error: `Erro na busca do artista: ${err}`
                    })
                }
            )
        },
        getMusicsByArtistId: (req,res) => {
            artistRepository.getMusicsByArtistId(req.params.id).then(
                musics => {
                    if(musics.data){
                        res.status(200).json({
                            success: true,
                            musics: musics.data
                        })
                    }else{
                        res.json({
                            success: false,
                            error: musics.error
                        })
                    }
                    
                },
                err => {
                    res.status(403).json({
                        success: false,
                        error: `Erro na busca do artista: ${err}`
                    })
                }
            )
        }
    }
    return ArtistController;
}