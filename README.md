## Start a project

You’ll start by executing "npm run start" inside the project folder

---

## GET Artists Collection By UserID

http://localhost:3000/api/user/(userId)/artists

---

## GET Artist Details

http://localhost:3000/api/artist/(artistId)

---

## GET Artist Musics with Previews

http://localhost:3000/api/artist/(artistId)/musics

---

## Start a TESTS

1 - Start a project with "npm run start"
2 - You’ll tests by executing "npm run test" inside the project folder

---